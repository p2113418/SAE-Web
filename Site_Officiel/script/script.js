// Fonction sticky header 

window.onscroll = function () { myFunction() };

var header = document.getElementById("bar_cool");
var sticky = header.offsetTop;
function myFunction() {
    if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
    } else {
        header.classList.remove("sticky");
    }
}

//Fonction ajoute change a menu et ajoute la propriete hidden à overflow-y
const body = document.querySelector("body");

function animationmenu(x) {
    x.classList.toggle("change");
    if (!x.classList.contains("change")) {
        // enable scroll
        body.style.overflow = "auto";
    } else {
        // disable scroll
        body.style.overflow = "hidden";
    }
}


// Fonction News

const timeToChange = 10//Temps pour que les images défiles en secondes
document.querySelector('.carousel').style = `--timeToChange : ${timeToChange}s;`

const imgs = Array.from(document.querySelectorAll('.carousel .img-container img'))

const texts = Array.from(document.querySelectorAll('.carousel .img-desc-container p'))

const nextImage = (addToIndex = 0) => {
    let currentActiveIndex;
    imgs.some((img, index) => { currentActiveIndex = index; return img.classList.contains('active') })
    let nextIndex = currentActiveIndex + 1 + addToIndex
    nextIndex = (nextIndex === imgs.length) ? 0 : (nextIndex < 0) ? imgs.length - 1 : nextIndex
    imgs[currentActiveIndex].classList.toggle('active')
    imgs[nextIndex].classList.toggle('active')
}

const nextText = (addToIndex = 0) => {
    let currentActiveIndex;
    texts.some((p, index) => { currentActiveIndex = index; return p.classList.contains('active') })
    let nextIndex = currentActiveIndex + 1 + addToIndex
    nextIndex = (nextIndex === texts.length) ? 0 : (nextIndex < 0) ? texts.length - 1 : nextIndex
    texts[currentActiveIndex].classList.toggle('active')
    texts[nextIndex].classList.toggle('active')
}

const interval = setInterval(nextImage, timeToChange * 1000)

const newinterval = setInterval(nextText, timeToChange * 1000)


document.querySelector('.control .arrow-left').addEventListener('click', () => {
    nextImage(-2)
    nextText(-2)
})

document.querySelector('.control .arrow-right').addEventListener('click', () => {
    nextImage()
    nextText()
})

//Fonction animation de taux de reussite

function animateValue(obj, start, end, duration) {
    let startTimestamp = null;
    const step = (timestamp) => {
        if (!startTimestamp) startTimestamp = timestamp;
        const progress = Math.min((timestamp - startTimestamp) / duration, 1);
        obj.innerHTML = Math.floor(progress * (end - start) + start);
        if (progress < 1) {
            window.requestAnimationFrame(step);
        }
    };
    window.requestAnimationFrame(step);
}

const obj = document.getElementById("value");

//Scroll Event listener
var scrollpos = window.scrollY; // window scroll position
var wh = window.innerHeight - 50; // as soon as element touches bottom with offset event starts
var element = document.querySelector("div.reussite"); //element

window.addEventListener('scroll', function () {
    if (scrollpos < (element.offsetTop - wh)) {
        animateValue(obj, 0, 90, 2000);

    }

});
